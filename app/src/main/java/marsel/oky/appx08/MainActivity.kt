package marsel.oky.appx08

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main.*
import java.util.prefs.AbstractPreferences

class MainActivity : AppCompatActivity() {

    lateinit var preferences : SharedPreferences
    val RC_SUKSES : Int = 100
    val PREF_NAME = "Setting"
    val FIELD_FONT_SIZE_SUBJUDUL = "Font_Size_Subjudul"
    val FIELD_FONT_SIZE_DETAIL = "Font_Size_Detail"
    val FIELD_BG = "BACKGROUND"
    val FIELD_FONT_COLOR_HEADER = "COLOR_HEADER"
    val FIELD_TEXT = "text"
    var bg : String = ""
    var colorHead : String =""
    var fDetail : Int = 14
    var fSubjudul : Int = 24
    var judul : String = "Hello World"
    var detail : String = "Film ini menceritakan Kyoto di tahun 2027, Naomi Katagaki, seorang murid SMA bertemu dengan seorang laki-laki yang mengaku sebagai dirinya dari 10 tahun yang akan datang. Bersama-sama mereka harus merubah masa depan untuk menyelamatkan salah seorang murid perempuan yang akan ia cintai tiga bulan yang akan datang."
    val DEF_FONT_SIZE_SUBJUDUL = fSubjudul
    val DEF_FONT_SIZE_DETAIL = fDetail
    val DEF_JUDUL = judul
    val DEF_BG = bg
    val DEF_FONT_COLOR_HEADER = colorHead
    val DEF_DETAIL = detail

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        tx_judul.setText(preferences.getString(FIELD_TEXT,DEF_JUDUL))
        tx_isi.setText(preferences.getString(FIELD_TEXT,DEF_DETAIL))
        fSubjudul = preferences.getInt(FIELD_FONT_SIZE_SUBJUDUL,DEF_FONT_SIZE_SUBJUDUL)
        fDetail = preferences.getInt(FIELD_FONT_SIZE_DETAIL,DEF_FONT_SIZE_DETAIL)
        bg = preferences.getString(FIELD_BG,DEF_BG).toString()
        colorHead = preferences.getString(FIELD_FONT_COLOR_HEADER,DEF_FONT_COLOR_HEADER).toString()

        background()
        headerColor()
        UkDetail()
        UkSubjudul()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == RC_SUKSES)
                tx_judul.setText(data?.extras?.getString("judul"))
                tx_isi.setText(data?.extras?.getString("detail"))
            bg = data?.extras?.getString("bgColor").toString()
            colorHead = data?.extras?.getString("headColor").toString()
            fSubjudul = data?.extras?.getInt("textSubjudul").toString().toInt()
            fDetail = data?.extras?.getInt("textDetail").toString().toInt()
            preferences = getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE)
            val prefEdit = preferences.edit()
            prefEdit.putInt(FIELD_FONT_SIZE_SUBJUDUL,fSubjudul)
            prefEdit.putInt(FIELD_FONT_SIZE_DETAIL,fDetail)
            prefEdit.putString(FIELD_TEXT,judul)
            prefEdit.putString(FIELD_TEXT,detail)
            prefEdit.putString(FIELD_BG,bg)
            prefEdit.putString(FIELD_FONT_COLOR_HEADER,colorHead)
            prefEdit.commit()

            background()
            headerColor()
            UkDetail()
            UkSubjudul()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var MenuInflater = menuInflater
        MenuInflater.inflate(R.menu.option_menu,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.item_setting->{
                var intent = Intent(this,SettingActivity::class.java)
                intent.putExtra("judul",tx_judul.text.toString())
                intent.putExtra("detail",tx_isi.text.toString())
                startActivityForResult(intent,RC_SUKSES)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun background(){
        if(bg=="Blue"){
            constraintLayout.setBackgroundColor(Color.BLUE)
        }
        else if(bg=="Yellow"){
            constraintLayout.setBackgroundColor(Color.YELLOW)
        }
        else if(bg=="Green"){
            constraintLayout.setBackgroundColor(Color.GREEN)
        }
        else if(bg=="Black"){
            constraintLayout.setBackgroundColor(Color.BLACK)
        }
    }

    fun headerColor(){
        if(colorHead=="Blue"){
            tx_head.setTextColor(Color.BLUE)
        }
        else if(colorHead=="Yellow"){
            tx_head.setTextColor(Color.YELLOW)
        }
        else if(colorHead=="Green"){
            tx_head.setTextColor(Color.GREEN)
        }
        else if(colorHead=="Black"){
            tx_head.setTextColor(Color.BLACK)
        }
        else{
            tx_head.setTextColor(Color.RED)
        }
    }

    fun UkSubjudul(){
        tx_subjudul.textSize = fSubjudul.toFloat()
    }

    fun UkDetail(){
        tx_isi.textSize = fDetail.toFloat()
    }

}
